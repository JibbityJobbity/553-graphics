#pragma once
#include "Headers.h"

// To load a texture, this routine must be called twice
// 1st call (phase = 1 - opens the file and returns size information (extWidth and extHeight); leaving the file open
// 2nd call (phase = 2) - call with pointer to texture buffer, and row layout information; loads data and closes file
// If the caller decides not to proceed with texture loading after the first call, it must call this routine again
// with phase set to 3 and other parameters null or zero to close the file

bool LoadBMPFile(const char *filename, int phase, int *extWidth, int *extHeight, UINT8 *destPtr, UINT rowCount, UINT rowLength, UINT rowPitch);
