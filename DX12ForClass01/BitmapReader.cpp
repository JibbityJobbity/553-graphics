#include "BitmapReader.h"

static bool LoadBMPFileReadHeader(FILE *&bm, BITMAPFILEHEADER &fileHeader, BITMAPINFOHEADER &header, const char *filename, int &width, int &height)
{
	if (fread_s(&fileHeader, sizeof(fileHeader), sizeof(fileHeader), 1, bm) != 1) {
		printf("Couldn't read BITMAPFILEHEADER\n");
		return false;
	}
	if (fread_s(&header, sizeof(header), sizeof(header), 1, bm) != 1) {
		printf("Couldn't read bitmap file info header\n");
		return false;
	}
	if (header.biCompression != 0) {
		printf("Can't handle texture file compression\n");
		return false;
	}
	if (header.biBitCount != 24) {
		printf("Can only handle 24 bit BMP files\n");
		return false;
	}
	if (header.biPlanes != 1) {
		printf("Can only single pixel plane in BMP files\n");
		return false;
	}
	if (sizeof(fileHeader) + sizeof(header) != fileHeader.bfOffBits) {
		printf("Texture BMP file header sizes not as expected\n");
		return false;
	}
	width = header.biWidth;
	height = header.biHeight;
	return true;
}

static bool LoadBMPFileReadData(FILE *&bm, BITMAPFILEHEADER &fileHeader, BITMAPINFOHEADER &header, UINT8 *destPtr, UINT rowCount, UINT rowLength, UINT rowPitch)
{
	UINT rowPixelCount = rowLength / 4;  // Also difference in line length between 24 and 32 bits/pixel
	UINT inputRowLength = rowLength - rowPixelCount;   // Reading 24 bit/pixel data
	UINT8 *readDestPtr, *finalDestPtr;
	for (UINT y = 0; y < rowCount; y++) {
		// Read data into the top 3/4 of destination line
		readDestPtr = destPtr + rowPixelCount;
		finalDestPtr = destPtr;
		if (fread_s(readDestPtr, inputRowLength, inputRowLength, 1, bm) != 1)
			return false;
		// Copy data down into place, adding alpha channel to each pixel
		for (UINT p = 0; p < rowPixelCount; p++) {
			*finalDestPtr++ = *readDestPtr++;
			*finalDestPtr++ = *readDestPtr++;
			*finalDestPtr++ = *readDestPtr++;
			*finalDestPtr++ = 255;  // Alpha
		}
		destPtr += rowPitch;
	}
	return true;
}

bool LoadBMPFile(const char *filename, int phase, int *extWidth, int *extHeight, UINT8 *destPtr, UINT rowCount, UINT rowLength, UINT rowPitch)
{
	static BITMAPFILEHEADER fileHeader;
	static BITMAPINFOHEADER header;
	static FILE *bm = NULL;
	static int width, height;
	static int lastPhase = 0;
	bool success;

	switch (phase) {
	case 1:  // First phase is to open the file and read the headers, returning width and height
		if (lastPhase != 0) return false;
		if (fopen_s(&bm, filename, "rb") != S_OK) {
			printf("Couldn't open file \"%s\"\n", filename);
			return false;
		}
		success = LoadBMPFileReadHeader(bm, fileHeader, header, filename, width, height);
		if (!success) {
			fclose(bm);
			bm = NULL;
			lastPhase = 0;
			return false;
		}
		lastPhase = phase;
		*extWidth = width;
		*extHeight = height;
		return true;  // leaving file open
	case 2:  // Second phase assumes file open, reads data and packs into texture upload buffer
		if (lastPhase != 1) return false;
		success = LoadBMPFileReadData(bm, fileHeader, header, destPtr, rowCount, rowLength, rowPitch);
		fclose(bm);
		bm = NULL;
		lastPhase = 0;
		return success;
	case 3:  // Shut down if external code decides not to proceed with load
		if (bm != NULL) {
			fclose(bm);
			bm = NULL;
			lastPhase = 0;
		}
	}
	return false;
}
