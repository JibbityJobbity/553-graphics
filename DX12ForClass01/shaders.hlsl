#define MAX_LIGHTS 8

struct FragmentInput
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	float4 worldPosition : WORLD_POSITION;
	float3 worldNormal : WORLD_NORMAL;
};

cbuffer ConstantBuffer : register(b0)
{
	float4x4 world;
	float4x4 view;
	float4x4 projection;
	float3 eyePos;
	float3 offset;
	float seconds;
	int ptLightCount;
	float4 ptLights[MAX_LIGHTS];
};

Texture2D g_texture_albedo : register(t0);
Texture2D g_texture_normal : register(t1);
SamplerState g_sampler_albedo : register(s0);
SamplerState g_sampler_normal : register(s1);


FragmentInput VertexMain(float4 position : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD)
{
	FragmentInput result;

	result.position = mul(mul(projection, mul(view, world)), position);
	//result.uv = ((1.0).xx - uv.yx)/ 2.0;
	result.uv = uv.yx / 2;
	result.worldNormal = normalize(mul((float3x3)world, normal));
	result.worldPosition = mul(world, position);

	return result;
}

float4 FragmentMain(FragmentInput input) : SV_TARGET
{
	float4 tex = g_texture_albedo.Sample(g_sampler_albedo, input.uv).zyxw;
	float4 norm = g_texture_normal.Sample(g_sampler_normal, input.uv).zyxw;

	float brightness = 0.2;
	//float3 debug = float3(0.0, 0.0, 0.0);
	float3 v = normalize(eyePos - (float3)input.worldPosition);
	float3 n = normalize(input.worldNormal);
	n = ((norm.xyz * 2.0) - 1);
	n.x = -n.x;
	float3 reflection = 2 * dot(v, n) * n - v;
	for (int i = 0; i < ptLightCount; i++) {
		// Diffuse
		float3 l = normalize(ptLights[i]);//normalize(input.worldPosition - ptLights[i]);
		brightness += 0.6 * saturate(dot(-l, n));
		// Specular
		//debug = reflection;
		brightness += 0.2 * pow(saturate(dot(-l , reflection)), 30);
	}
	//return float4(debug, 1.0);
	//return float4(input.uv, 0.0, 1.0).xxxw;
	return brightness * tex;
}

