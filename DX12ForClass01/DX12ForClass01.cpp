#include "Headers.h"
#include "BitmapReader.h"
#include "ObjReader.h"

using namespace DirectX;

// Settings
const int WIDTH = 800;
const int HEIGHT = 800;
const int BUFFER_COUNT = 2;
const int CUBE_COUNT = 2;
#define MAX_LIGHTS 8
#define TEX_COMPONENT_COUNT 2

// Structures
D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =              // Vertex input layout
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
};

struct Vertex { XMFLOAT3 position, normal; XMFLOAT2 uv; };
const UINT vertexSize = sizeof(Vertex);
UINT vertexCount;   // Will be set to number of vertices in model (used for drawing)

struct ConstantBuffer { XMMATRIX world, view, projection; XMVECTOR eyePos;  XMFLOAT3 offset; float seconds; int ptLightCount; XMVECTOR ptLights[MAX_LIGHTS]; };

ConstantBuffer constantBufferData;

// State
HWND MainWindowHandle;
int backBufferIndex;
UINT64 bufferFenceValue[BUFFER_COUNT] = { 0, 0 };
bool keyMap[256] = {};  // Set when key is down indexed by VK code
float heading = 0.0f, elevation = 0.0f;
XMVECTOR cameraPosition = { 0.0f, 0.0f, 4.0f };

///////////////////////////////////////////////////////////////////
// DirectX Resources needing global access in order of creation  //
///////////////////////////////////////////////////////////////////

// DirectX 12 pipeline resources (display buffers and structure)
ID3D12Debug *debugController = NULL;
IDXGIFactory4 *factory = NULL;
IDXGIAdapter1 *adapter = NULL;
ID3D12Device *device = NULL;
ID3D12CommandQueue *commandQueue = NULL;
IDXGISwapChain3 *swapChain = NULL;
ID3D12DescriptorHeap *rtvHeap = NULL;
unsigned int rtvDescriptorSize;
ID3D12DescriptorHeap* srvHeaps[CUBE_COUNT] = { NULL, NULL };
unsigned int srvDescriptorSize;
ID3D12DescriptorHeap *dsvHeap = NULL;
ID3D12Resource *renderTargets[BUFFER_COUNT] = { NULL, NULL };
ID3D12CommandAllocator *directCommandAllocator[BUFFER_COUNT] = { NULL, NULL };
ID3D12CommandAllocator *bundleCommandAllocator = NULL;
ID3D12Resource *depthBuffer = NULL;

// Command list
ID3D12GraphicsCommandList *commandList = NULL;

// Assets
ID3D12RootSignature *rootSignature = NULL;
ID3D12PipelineState *pipelineState = NULL;
ID3D12Resource *vertexBuffer = NULL;
D3D12_VERTEX_BUFFER_VIEW vertexBufferView;
struct TextureBuffer { ID3D12Resource* texture = NULL; ID3D12Resource* textureUpload = NULL; };
TextureBuffer textureBuffers[TEX_COMPONENT_COUNT];
std::string componentNames[TEX_COMPONENT_COUNT] = {
	"/albedo.bmp",
	"/normal.bmp"
};
ID3D12Resource* constantBuffers[CUBE_COUNT] = { NULL, NULL };
UINT8 *constantBufferMappedPointers[CUBE_COUNT];

// Prerecorded commands
ID3D12GraphicsCommandList *bundle;

// Synchronisation primitives
ID3D12Fence *fence = NULL;
HANDLE fenceEvent = NULL;

void ErrorExit()
{
	printf("Press <Enter> to exit\n");
	getchar();
	exit(-1);
}

template<class T>void FreeResource(T *&resource, const char *name, bool checkCount)
{
	if (resource != NULL) {
		int count = resource->Release();
		if (checkCount && count != 0) {
			printf("Resource count not zero(%d) on release for %s\n", count, name);
			ErrorExit();
		}
		resource = NULL;
	}
}

void FreeResources()
{
	// Synchronisation primitives
	if (fenceEvent != NULL) {
		CloseHandle(fenceEvent);
		fenceEvent = NULL;
	}
	FreeResource(fence, "Fence", true);

	// Assets
	for (int i = 0; i < CUBE_COUNT; i++) {
		if (constantBuffers[i] != NULL) constantBuffers[i]->Unmap(0, NULL);
		FreeResource(constantBuffers[i], "Constant Buffer", true);
	}
	FreeResource(bundle, "Command Bundle", true);
	for (int i = 0; i < TEX_COMPONENT_COUNT; i++) {
		FreeResource(textureBuffers[i].textureUpload, "Texture Upload Buffer", true);
		FreeResource(textureBuffers[i].texture, "Texture", true);
	}
	FreeResource(vertexBuffer, "Vertex Buffer", true);
	FreeResource(pipelineState, "Pipeline State", true);
	FreeResource(rootSignature, "Root Signature", true);

	// Command list
	FreeResource(commandList, "Command List", true);

	// Pipeline
	FreeResource(depthBuffer, "Depth Stencil Buffer", true);
	FreeResource(bundleCommandAllocator, "Bundle Command Allocator", true);
	for (int f = 0; f < BUFFER_COUNT; f++) {
		FreeResource(directCommandAllocator[f], "Command Allocator", true);
		FreeResource(renderTargets[f], "Render Target", f == BUFFER_COUNT - 1);
	}
	FreeResource(dsvHeap, "Depth Stencil View Heap", true);
	for (int i = 0; i < CUBE_COUNT; i++)
		FreeResource(srvHeaps[i], "Shader Resource Descriptor Heap", true);
	FreeResource(rtvHeap, "Render Target Descriptor Heap", true);
	FreeResource(swapChain, "Swap Chain", true);
	FreeResource(commandQueue, "Command Queue", true);
	FreeResource(device, "Graphics Device", true);
	FreeResource(adapter, "Graphics Adapter", true);
	FreeResource(factory, "DXGI Factory", true);
	FreeResource(debugController, "Debug Interface", true);
}

void FreeAndExit(const char* message)
{
	printf("Error: %s\n", message);
	FreeResources();
	ErrorExit();
}

void WaitForCommandCompletion(UINT64 fenceValue)
{
	if (commandQueue->Signal(fence, fenceValue) != S_OK)
		FreeAndExit("Couldn't set signal on fence in command queue");

	if (fence->GetCompletedValue() < fenceValue) {
		if (fence->SetEventOnCompletion(fenceValue, fenceEvent))
			FreeAndExit("Couldn't set event on fence");
		WaitForSingleObject(fenceEvent, INFINITE);
	}
}

void CreateDirectX12Pipeline()
{
	/* Enable the D3D12 debug layer */ {
		if (D3D12GetDebugInterface(__uuidof(ID3D12Debug), (void **)&debugController) != S_OK)
			FreeAndExit("Couldn't create debug layer");
		debugController->EnableDebugLayer();
	}

	/* Create the device */ {
		if (CreateDXGIFactory2(0, __uuidof(IDXGIFactory4), (void **)&factory) != S_OK)
			FreeAndExit("Couldn't create IDXGIFactory4");

		// Scan available adapters to find an NVIDIA hardware adapter
		for (unsigned int i = 0; factory->EnumAdapters1(i, &adapter) != DXGI_ERROR_NOT_FOUND; i++) {
			printf("Found adapter %d\n", i);
			DXGI_ADAPTER_DESC1 desc;
			adapter->GetDesc1(&desc);
			printf("  Description: %S\n", desc.Description);
			printf("  Kind: %s\n", desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE ? "Software" : "Hardware");
			printf("  Video memory: %lx\n", desc.DedicatedVideoMemory);
			printf("  System memory: %lx\n", desc.DedicatedSystemMemory);
			if ((desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) == 0) {  // was NVIDIA
				break;
			}
			FreeResource(adapter, "Graphics adapter", true);
		}

		if (adapter == NULL)
			FreeAndExit("Couldn't find an NVIDIA hardware graphics adapter");  // Text modified for laptop

		// Create Graphics device checking that the adapter supports DirectX 12.1 features
		if (D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_12_1, _uuidof(ID3D12Device), (void **)&device) != S_OK)
			FreeAndExit("NVIDIA graphics adapter doesn't support DirectX 12.1");
	}

	/* Describe and create the command queue */ {
		D3D12_COMMAND_QUEUE_DESC qdesc;
		qdesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
		qdesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
		qdesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
		qdesc.NodeMask = 0;
		if (device->CreateCommandQueue(&qdesc, __uuidof(ID3D12CommandQueue), (void **)&commandQueue) != S_OK)
			FreeAndExit("Couldn't create Command Queue");
	}

	/* Describe and create the swap chain */ {
		DXGI_SWAP_CHAIN_DESC1 scdesc;
		scdesc.Width = WIDTH;
		scdesc.Height = HEIGHT;
		scdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scdesc.Stereo = FALSE;
		scdesc.SampleDesc.Count = 1;
		scdesc.SampleDesc.Quality = 0;
		scdesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scdesc.BufferCount = BUFFER_COUNT;
		scdesc.Scaling = DXGI_SCALING_STRETCH;
		scdesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		scdesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
		scdesc.Flags = 0;

		IDXGISwapChain1 *sctemp = NULL;
		if (factory->CreateSwapChainForHwnd(commandQueue, MainWindowHandle, &scdesc, NULL, NULL, &sctemp) != S_OK)
			FreeAndExit("Couldn't create Swap Chain");

		// This sample does not support fullscreen transitions.
		if (factory->MakeWindowAssociation(MainWindowHandle, DXGI_MWA_NO_ALT_ENTER) != S_OK)
			FreeAndExit("Couldn't block fullscreen transitions");

		if (sctemp->QueryInterface(__uuidof(IDXGISwapChain3), (void **)&swapChain) != S_OK)
			FreeAndExit("Couldn't get SwapChain3 interface from SwapChain1 object");

		backBufferIndex = swapChain->GetCurrentBackBufferIndex();
		sctemp->Release();
		sctemp = NULL;
	}

	/* Create descriptor heaps */ {
		// Heap for render target views
		D3D12_DESCRIPTOR_HEAP_DESC rtvhdesc;
		rtvhdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvhdesc.NumDescriptors = BUFFER_COUNT;
		rtvhdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		rtvhdesc.NodeMask = 0;
		if (device->CreateDescriptorHeap(&rtvhdesc, __uuidof(ID3D12DescriptorHeap), (void**)&rtvHeap) != S_OK)
			FreeAndExit("Couldn't create render target descriptor heap");
		rtvDescriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		// Heap for shader resource views (texture / constants)
		D3D12_DESCRIPTOR_HEAP_DESC srvhdesc;
		srvhdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		srvhdesc.NumDescriptors = 3;
		srvhdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		srvhdesc.NodeMask = 0;
		for (int i = 0; i < CUBE_COUNT; i++) {
			if (device->CreateDescriptorHeap(&srvhdesc, __uuidof(ID3D12DescriptorHeap), (void**)&srvHeaps[i]) != S_OK)
				FreeAndExit("Couldn't create shader resource view descriptor heap");
		}
		srvDescriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		// Heap for depth stencil view
		D3D12_DESCRIPTOR_HEAP_DESC dsvhdesc;
		dsvhdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		dsvhdesc.NumDescriptors = 1;
		dsvhdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		dsvhdesc.NodeMask = 0;
		if (device->CreateDescriptorHeap(&dsvhdesc, __uuidof(ID3D12DescriptorHeap), (void **)&dsvHeap) != S_OK)
			FreeAndExit("Couldn't create depth stencil view descriptor heap");
	}

	/* Create frame resource and command allocator for each render frame */ {
		D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptor = rtvHeap->GetCPUDescriptorHandleForHeapStart();
		for (int f = 0; f < BUFFER_COUNT; f++) {
			if (swapChain->GetBuffer(f, __uuidof(ID3D12Resource), (void **)&renderTargets[f]) != S_OK)
				FreeAndExit("Couldn't get swap chain buffer");
			device->CreateRenderTargetView(renderTargets[f], NULL, rtvCPUDescriptor);
			rtvCPUDescriptor.ptr += rtvDescriptorSize;

			if (device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void **)&directCommandAllocator[f]) != S_OK)
				FreeAndExit("Couldn't create direct command allocator");
		}
		// Bundle command allocator
		if (device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, __uuidof(ID3D12CommandAllocator), (void **)&bundleCommandAllocator) != S_OK)
			FreeAndExit("Couldn't create bundle command allocator");
	}

	/* Create depth stencil buffer and descriptor */ {
		D3D12_HEAP_PROPERTIES dhprop;
		dhprop.Type = D3D12_HEAP_TYPE_DEFAULT;
		dhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		dhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		dhprop.CreationNodeMask = 0;
		dhprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC ddesc;
		ddesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		ddesc.Alignment = 0;
		ddesc.Width = WIDTH;
		ddesc.Height = HEIGHT;
		ddesc.DepthOrArraySize = 1;
		ddesc.MipLevels = 1;
		ddesc.Format = DXGI_FORMAT_D32_FLOAT;
		ddesc.SampleDesc.Count = 1;
		ddesc.SampleDesc.Quality = 0;
		ddesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
		ddesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

		if (device->CreateCommittedResource(&dhprop, D3D12_HEAP_FLAG_NONE, &ddesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, NULL, __uuidof(ID3D12Resource), (void **)&depthBuffer) != S_OK) {
			FreeAndExit("Couldn't depth buffer");
		}

		D3D12_DEPTH_STENCIL_VIEW_DESC dsvdesc;
		dsvdesc.Format = ddesc.Format;
		dsvdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		dsvdesc.Flags = D3D12_DSV_FLAG_NONE;
		dsvdesc.Texture2D.MipSlice = 0;

		device->CreateDepthStencilView(depthBuffer, &dsvdesc, dsvHeap->GetCPUDescriptorHandleForHeapStart());
	}
}

void LoadTexture(const char* name) {
	for (int i = 0; i < TEX_COMPONENT_COUNT; i++) {
		// Start reading the texture file, to obtain width and height
		int textureWidth, textureHeight;
		if (!LoadBMPFile((std::string(name) + componentNames[i]).c_str(), 1, &textureWidth, &textureHeight, NULL, 0, 0, 0))
			FreeAndExit("Couldn't load texture file");

		// Create the texture buffer
		D3D12_HEAP_PROPERTIES texhprop;
		texhprop.Type = D3D12_HEAP_TYPE_DEFAULT;
		texhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		texhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		texhprop.CreationNodeMask = 0;
		texhprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC texdesc;  // texture resource description
		texdesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		texdesc.Alignment = 0;
		texdesc.Width = textureWidth;
		texdesc.Height = textureHeight;
		texdesc.DepthOrArraySize = 1;
		texdesc.MipLevels = 1;
		texdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texdesc.SampleDesc.Count = 1;
		texdesc.SampleDesc.Quality = 0;
		texdesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;  // TODO ???? should this be row major
		texdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

		if (device->CreateCommittedResource(&texhprop, D3D12_HEAP_FLAG_NONE, &texdesc, D3D12_RESOURCE_STATE_COPY_DEST, NULL, __uuidof(ID3D12Resource), (void**)&textureBuffers[i].texture) != S_OK) {
			LoadBMPFile("", 3, NULL, NULL, NULL, 0, 0, 0);
			FreeAndExit("Couldn't create buffer resource for texture");
		}

		// Determine size and layout for texture upload buffer
		D3D12_PLACED_SUBRESOURCE_FOOTPRINT footprint;
		UINT rowCount;
		UINT64 rowLength;
		UINT64 uploadReqdSize = 0;
		device->GetCopyableFootprints(&texdesc, 0, 1, 0, &footprint, &rowCount, &rowLength, &uploadReqdSize);

		// Create texture upload buffer
		D3D12_HEAP_PROPERTIES upprop;
		upprop.Type = D3D12_HEAP_TYPE_UPLOAD;
		upprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		upprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		upprop.CreationNodeMask = 0;
		upprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC updesc;
		updesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		updesc.Alignment = 0;
		updesc.Width = uploadReqdSize;
		updesc.Height = 1;
		updesc.DepthOrArraySize = 1;
		updesc.MipLevels = 1;
		updesc.Format = DXGI_FORMAT_UNKNOWN;
		updesc.SampleDesc.Count = 1;
		updesc.SampleDesc.Quality = 0;
		updesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		updesc.Flags = D3D12_RESOURCE_FLAG_NONE;

		if (device->CreateCommittedResource(&upprop, D3D12_HEAP_FLAG_NONE, &updesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void**)&textureBuffers[i].textureUpload) != S_OK) {
			LoadBMPFile("", 3, NULL, NULL, NULL, 0, 0, 0);  // just close the file
			FreeAndExit("Couldn't create texture upload buffer");
		}

		// Map upload buffer and read texture data directly into it
		UINT8* uploadPtr;
		D3D12_RANGE readRange;
		readRange.Begin = 0;
		readRange.End = 0;
		if (textureBuffers[i].textureUpload->Map(0, &readRange, (void**)&uploadPtr) != S_OK) {
			LoadBMPFile("", 3, NULL, NULL, NULL, 0, 0, 0);
			FreeAndExit("Couldn't map texture upload buffer");
		}
		if (!LoadBMPFile("", 2, NULL, NULL, uploadPtr + footprint.Offset, rowCount, (UINT)rowLength, footprint.Footprint.RowPitch)) {
			textureBuffers[i].textureUpload->Unmap(0, NULL);
			FreeAndExit("Couldn't read texture data from file");
		}
		textureBuffers[i].textureUpload->Unmap(0, NULL);  // TODO see if a small write range will work

										// Schedule a copy from texture upload buffer to texture buffer
		D3D12_TEXTURE_COPY_LOCATION srcLoc;
		srcLoc.pResource = textureBuffers[i].textureUpload;
		srcLoc.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
		srcLoc.PlacedFootprint = footprint;

		D3D12_TEXTURE_COPY_LOCATION destLoc;
		destLoc.pResource = textureBuffers[i].texture;
		destLoc.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
		destLoc.SubresourceIndex = 0;

		commandList->CopyTextureRegion(&destLoc, 0, 0, 0, &srcLoc, NULL);

		// Add resource barrier to transition texture to pixel resource
		D3D12_RESOURCE_BARRIER barrier;
		barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
		barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
		barrier.Transition.pResource = textureBuffers[i].texture;
		barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
		barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
		commandList->ResourceBarrier(1, &barrier);

		// Create a Shader Resource View for the texture
		D3D12_SHADER_RESOURCE_VIEW_DESC srvdesc;
		srvdesc.Format = texdesc.Format;
		srvdesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		srvdesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;  // Swizzle colours
		srvdesc.Texture2D.MostDetailedMip = 0;
		srvdesc.Texture2D.MipLevels = 1;
		srvdesc.Texture2D.PlaneSlice = 0;
		srvdesc.Texture2D.ResourceMinLODClamp = 0.0f;
		for (int j = 0; j < CUBE_COUNT; j++) {
			D3D12_CPU_DESCRIPTOR_HANDLE currentHeapHandle = srvHeaps[j]->GetCPUDescriptorHandleForHeapStart();
			currentHeapHandle.ptr += (i==1 ? 2 : 0) * srvDescriptorSize; // Using pos 0 or 2
			device->CreateShaderResourceView(textureBuffers[i].texture, &srvdesc, currentHeapHandle);
		}
	}
}

void CreateAssets() {
	/* Create root signature */ {
		D3D12_DESCRIPTOR_RANGE range[3];
		// range[0] is used for Albedo Texture
		range[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
		range[0].NumDescriptors = 1;
		range[0].BaseShaderRegister = 0;
		range[0].RegisterSpace = 0;
		range[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

		// range[1] is used for Constant Buffer
		range[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
		range[1].NumDescriptors = 1;
		range[1].BaseShaderRegister = 0;
		range[1].RegisterSpace = 0;
		range[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

		// range[2] is used for Normal Texture
		range[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
		range[2].NumDescriptors = 1;
		range[2].BaseShaderRegister = 1;
		range[2].RegisterSpace = 0;
		range[2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

		D3D12_ROOT_PARAMETER params[1];
		params[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
		params[0].DescriptorTable.NumDescriptorRanges = 3;
		params[0].DescriptorTable.pDescriptorRanges = range;
		params[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		D3D12_STATIC_SAMPLER_DESC samplers[TEX_COMPONENT_COUNT];
		samplers[0].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
		samplers[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		samplers[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		samplers[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		samplers[0].MipLODBias = 0.0f;
		samplers[0].MaxAnisotropy = 0;
		samplers[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
		samplers[0].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
		samplers[0].MinLOD = 0.0f;
		samplers[0].MaxLOD = D3D12_FLOAT32_MAX;
		samplers[0].ShaderRegister = 0;
		samplers[0].RegisterSpace = 0;
		samplers[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

		samplers[1].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
		samplers[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		samplers[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		samplers[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		samplers[1].MipLODBias = 0.0f;
		samplers[1].MaxAnisotropy = 0;
		samplers[1].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
		samplers[1].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
		samplers[1].MinLOD = 0.0f;
		samplers[1].MaxLOD = D3D12_FLOAT32_MAX;
		samplers[1].ShaderRegister = 1;
		samplers[1].RegisterSpace = 0;
		samplers[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

		D3D12_ROOT_SIGNATURE_DESC rsdesc;
		rsdesc.NumParameters = 1;
		rsdesc.pParameters = params;
		rsdesc.NumStaticSamplers = TEX_COMPONENT_COUNT;
		rsdesc.pStaticSamplers = samplers;
		rsdesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
		ID3DBlob *sigBlob = NULL, *errBlob = NULL;
		if (D3D12SerializeRootSignature(&rsdesc, D3D_ROOT_SIGNATURE_VERSION_1, &sigBlob, &errBlob) != S_OK) {
			if (errBlob != NULL) {
				printf("Error: Failed to serialize empty root signature\n");
				printf("Message: %s", (char *)errBlob->GetBufferPointer());
				errBlob->Release();
			}
			if (sigBlob != NULL) sigBlob->Release();
			FreeAndExit("Couldn't serialize root signature");
		}
		if (device->CreateRootSignature(0, sigBlob->GetBufferPointer(), sigBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void **)&rootSignature) != S_OK) {
			sigBlob->Release();
			FreeAndExit("Couldn't create root signature");
		}
		if (errBlob != NULL) errBlob->Release();
		sigBlob->Release();
	}

	ID3DBlob *vertexShaderBlob = NULL, *fragmentShaderBlob = NULL;
	/* Compile shaders */ {
		// TODO:  Investigate - looks as though I can still compile a complete effect
		// https://docs.microsoft.com/en-us/windows/desktop/direct3dhlsl/d3dcompilefromfile
		ID3DBlob *errBlob = NULL;
		UINT compileFlags = 0; //|| D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
		if (D3DCompileFromFile(L"shaders.hlsl", NULL, NULL, "VertexMain", "vs_5_0", compileFlags, 0, &vertexShaderBlob, &errBlob) != S_OK) {
			if (errBlob != NULL) {
				printf("Error: %s", (char *)errBlob->GetBufferPointer());
				errBlob->Release();
			}
			if (vertexShaderBlob != NULL) vertexShaderBlob->Release();
			FreeAndExit("Couldn't compile vertex shader");
		}
		if (D3DCompileFromFile(L"shaders.hlsl", NULL, NULL, "FragmentMain", "ps_5_0", compileFlags, 0, &fragmentShaderBlob, &errBlob) != S_OK) {
			if (errBlob != NULL) {
				printf("Error: Failed to compile fragment shader\n");
				printf("Message: %s", (char *)errBlob->GetBufferPointer());
				errBlob->Release();
			}
			vertexShaderBlob->Release();
			if (fragmentShaderBlob != NULL) fragmentShaderBlob->Release();
			FreeAndExit("Couldn't compile fragment shader");
		}
	}

	/* Create pipeline state object */ {
		D3D12_GRAPHICS_PIPELINE_STATE_DESC psodesc;
		psodesc.pRootSignature = rootSignature;

		psodesc.VS.pShaderBytecode = vertexShaderBlob->GetBufferPointer();
		psodesc.VS.BytecodeLength = vertexShaderBlob->GetBufferSize();
		psodesc.PS.pShaderBytecode = fragmentShaderBlob->GetBufferPointer();
		psodesc.PS.BytecodeLength = fragmentShaderBlob->GetBufferSize();
		psodesc.DS.pShaderBytecode = NULL;
		psodesc.DS.BytecodeLength = 0;
		psodesc.HS.pShaderBytecode = NULL;
		psodesc.HS.BytecodeLength = 0;
		psodesc.GS.pShaderBytecode = NULL;
		psodesc.GS.BytecodeLength = 0;

		psodesc.StreamOutput.pSODeclaration = NULL;
		psodesc.StreamOutput.NumEntries = 0;
		psodesc.StreamOutput.pBufferStrides = NULL;
		psodesc.StreamOutput.NumStrides = 0;
		psodesc.StreamOutput.RasterizedStream = 0;

		psodesc.BlendState.AlphaToCoverageEnable = FALSE;
		psodesc.BlendState.IndependentBlendEnable = FALSE;
		for (int rt = 0; rt < 8; rt++) {
			psodesc.BlendState.RenderTarget[rt].BlendEnable = FALSE;
			psodesc.BlendState.RenderTarget[rt].LogicOpEnable = FALSE;
			psodesc.BlendState.RenderTarget[rt].SrcBlend = D3D12_BLEND_ONE;
			psodesc.BlendState.RenderTarget[rt].DestBlend = D3D12_BLEND_ZERO;
			psodesc.BlendState.RenderTarget[rt].BlendOp = D3D12_BLEND_OP_ADD;
			psodesc.BlendState.RenderTarget[rt].SrcBlendAlpha = D3D12_BLEND_ONE;
			psodesc.BlendState.RenderTarget[rt].DestBlendAlpha = D3D12_BLEND_ZERO;
			psodesc.BlendState.RenderTarget[rt].BlendOpAlpha = D3D12_BLEND_OP_ADD;
			psodesc.BlendState.RenderTarget[rt].LogicOp = D3D12_LOGIC_OP_NOOP;
			psodesc.BlendState.RenderTarget[rt].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
		}

		psodesc.SampleMask = UINT_MAX;

		psodesc.RasterizerState.FillMode = D3D12_FILL_MODE_SOLID;
		psodesc.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;
		psodesc.RasterizerState.FrontCounterClockwise = TRUE;
		psodesc.RasterizerState.DepthBias = 0;
		psodesc.RasterizerState.DepthBiasClamp = 0.0f;
		psodesc.RasterizerState.SlopeScaledDepthBias = 0.0f;
		psodesc.RasterizerState.DepthClipEnable = TRUE;
		psodesc.RasterizerState.MultisampleEnable = FALSE;
		psodesc.RasterizerState.AntialiasedLineEnable = FALSE;
		psodesc.RasterizerState.ForcedSampleCount = 0;
		psodesc.RasterizerState.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

		psodesc.DepthStencilState.DepthEnable = TRUE;
		psodesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		psodesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
		psodesc.DepthStencilState.StencilEnable = FALSE;
		psodesc.DepthStencilState.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
		psodesc.DepthStencilState.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;
		psodesc.DepthStencilState.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
		psodesc.DepthStencilState.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
		psodesc.DepthStencilState.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
		psodesc.DepthStencilState.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
		psodesc.DepthStencilState.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
		psodesc.DepthStencilState.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
		psodesc.DepthStencilState.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
		psodesc.DepthStencilState.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

		psodesc.InputLayout.pInputElementDescs = inputElementDescs;
		psodesc.InputLayout.NumElements = 3;

		psodesc.IBStripCutValue = D3D12_INDEX_BUFFER_STRIP_CUT_VALUE_DISABLED;
		psodesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		psodesc.NumRenderTargets = 1;
		psodesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		for (int rt = 0; rt < 8; rt++) {
			psodesc.RTVFormats[rt] = DXGI_FORMAT_UNKNOWN;
		}
		psodesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		psodesc.SampleDesc.Count = 1;
		psodesc.SampleDesc.Quality = 0;
		psodesc.NodeMask = 0;
		psodesc.CachedPSO.pCachedBlob = NULL;
		psodesc.CachedPSO.CachedBlobSizeInBytes = 0;
		psodesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

		if (device->CreateGraphicsPipelineState(&psodesc, __uuidof(ID3D12PipelineState), (void **)&pipelineState) != S_OK) {
			fragmentShaderBlob->Release();
			vertexShaderBlob->Release();
			FreeAndExit("Couldn't create pipeline state");
		}
	}
	fragmentShaderBlob->Release();
	vertexShaderBlob->Release();

	/* Create the vertex buffer, transfer data and make buffer view(description?) */ {
		FILE *modelFile;
		UINT sizeReqdFloats;
		UINT reqdChunkSpace;

		if (fopen_s(&modelFile, "cube.obj", "r") != 0)
			FreeAndExit("Couldn't open model file");

		if (!OFR1111CheckAndCountParser(modelFile, sizeReqdFloats, reqdChunkSpace)) {
			fclose(modelFile);
			FreeAndExit("Parsing error in model(obj) file\n");
		}
		UINT vertexBufferSize = sizeReqdFloats * sizeof(float);
		vertexCount = sizeReqdFloats / 8;

		D3D12_HEAP_PROPERTIES vbhprop;
		vbhprop.Type = D3D12_HEAP_TYPE_UPLOAD;
		vbhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		vbhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		vbhprop.CreationNodeMask = 0;
		vbhprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC vbdesc;
		vbdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		vbdesc.Alignment = 0;
		vbdesc.Width = vertexBufferSize;
		vbdesc.Height = 1;
		vbdesc.DepthOrArraySize = 1;
		vbdesc.MipLevels = 1;
		vbdesc.Format = DXGI_FORMAT_UNKNOWN;
		vbdesc.SampleDesc.Count = 1;
		vbdesc.SampleDesc.Quality = 0;
		vbdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		vbdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

		// Note - this is not the preferred method of filling a vertex buffer that will
		// be used more than once - should upload and transfer to a default heap area
		if (device->CreateCommittedResource(
			&vbhprop, D3D12_HEAP_FLAG_NONE, &vbdesc,
			D3D12_RESOURCE_STATE_GENERIC_READ, NULL,
			__uuidof(ID3D12Resource), (void **)&vertexBuffer) != S_OK) {
			fclose(modelFile);
			FreeAndExit("Couldn't create vertex buffer");
		}

		UINT8 *vertexBufferPtr;
		D3D12_RANGE readRange;
		readRange.Begin = 0;
		readRange.End = 0;
		if (vertexBuffer->Map(0, &readRange, (void **)&vertexBufferPtr) != S_OK) {
			fclose(modelFile);
			FreeAndExit("Couldn't map vertex buffer");
		}
		OFR2222CollectAndFillParser(modelFile, reqdChunkSpace, (float *)vertexBufferPtr);
		fclose(modelFile);
		vertexBuffer->Unmap(0, NULL);

		vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
		vertexBufferView.SizeInBytes = vertexBufferSize;
		vertexBufferView.StrideInBytes = vertexSize;
	}

	/* Load the texture */ 
	LoadTexture("basket");

	/* Placeholder - does nothing */ {}

	/* Create and load the constant buffer */ {
		D3D12_HEAP_PROPERTIES cbhprop;
		cbhprop.Type = D3D12_HEAP_TYPE_UPLOAD;
		cbhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		cbhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		cbhprop.CreationNodeMask = 0;
		cbhprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC cbdesc;
		cbdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		cbdesc.Alignment = 0;
		cbdesc.Width = 1024 * 64;
		cbdesc.Height = 1;
		cbdesc.DepthOrArraySize = 1;
		cbdesc.MipLevels = 1;
		cbdesc.Format = DXGI_FORMAT_UNKNOWN;
		cbdesc.SampleDesc.Count = 1;
		cbdesc.SampleDesc.Quality = 0;
		cbdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		cbdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

		for (int i = 0; i < CUBE_COUNT; i++) {
			if (device->CreateCommittedResource(&cbhprop, D3D12_HEAP_FLAG_NONE, &cbdesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void**)&constantBuffers[i]) != S_OK)
				FreeAndExit("Couldn't create constant(upload) buffer");
		}

		// Create constant buffer views
		for (int i = 0; i < CUBE_COUNT; i++) {
			D3D12_CONSTANT_BUFFER_VIEW_DESC bvdesc;
			bvdesc.BufferLocation = constantBuffers[i]->GetGPUVirtualAddress();
			bvdesc.SizeInBytes = (sizeof(ConstantBuffer) + 255) & ~255;
			D3D12_CPU_DESCRIPTOR_HANDLE srvHandle = srvHeaps[i]->GetCPUDescriptorHandleForHeapStart();
			srvHandle.ptr += srvDescriptorSize;
			device->CreateConstantBufferView(&bvdesc, srvHandle);

			// Set initial constant buffer values
			constantBufferData.world = XMMatrixIdentity();
			constantBufferData.view = XMMatrixLookAtRH({ 0, 4, 7 }, { 0, 0, 0 }, { 0, 1, 0 });
			constantBufferData.projection = XMMatrixPerspectiveFovRH(3.14159f / 4.0f, 1.0f, 0.01f, 10000.0f);
			constantBufferData.offset = { 0.0f, 0.0f, 0.0f };
			constantBufferData.seconds = 0.0f;
			constantBufferData.ptLightCount = 1;
			constantBufferData.ptLights[0] = { -1, -1, -1, 0 };
			constantBufferData.eyePos = { 0.0, 0.0, 0.0 };

			// Map and initialise the constant buffer
			D3D12_RANGE readRange;
			readRange.Begin = 0;
			readRange.End = 0;
			if (constantBuffers[i]->Map(0, &readRange, (void**)&constantBufferMappedPointers[i]) != S_OK)
				FreeAndExit("Couldn't map constant buffer");
			memcpy(constantBufferMappedPointers[i], &constantBufferData, sizeof(ConstantBuffer));
			// Leave constant buffer mapped for updating on each frame
		}
	}
}

void Init()
{
	////////////////////////// PIPELINE ///////////////////////////
	CreateDirectX12Pipeline();

	// Create command list
	if (device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, directCommandAllocator[backBufferIndex], NULL, __uuidof(ID3D12GraphicsCommandList), (void **)&commandList) != S_OK)
		FreeAndExit("Couldn't create command list");

	/////////////////////////// ASSETS ////////////////////////////
	CreateAssets();

	/* Close command list (texture copy command) and execute commands */ {
		if (commandList->Close() != S_OK)
			FreeAndExit("Couldn't close command list to process buffer copies");
		ID3D12CommandList *cl[] = { commandList };
		commandQueue->ExecuteCommandLists(1, cl);
	}

	/* Create and record command bundle */ {
		if (device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_BUNDLE, bundleCommandAllocator, pipelineState, __uuidof(ID3D12CommandList), (void **)&bundle) != S_OK)
			FreeAndExit("Couldn't create bundle command list");
		bundle->SetGraphicsRootSignature(rootSignature);
		bundle->IASetVertexBuffers(0, 1, &vertexBufferView);
		bundle->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		bundle->DrawInstanced(vertexCount, 1, 0, 0);
		if (bundle->Close() != S_OK)
			FreeAndExit("Couldn't close command bundle");
	}

	/* Create synchronisation objects */ {
		if (device->CreateFence(bufferFenceValue[backBufferIndex], D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12Fence), (void **)&fence) != S_OK)
			FreeAndExit("Couldn't create fence");
		bufferFenceValue[backBufferIndex]++;
		fenceEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		if (fenceEvent == NULL)
			FreeAndExit("Couldn't create event for fence");
	}

	WaitForCommandCompletion(bufferFenceValue[backBufferIndex]);
	bufferFenceValue[backBufferIndex]++;
	for (int i = 0; i < TEX_COMPONENT_COUNT; i++)
		FreeResource(textureBuffers[0].textureUpload, "Texture Upload Buffer", true);
}

void FillCommandList()
{
	// Can only be done when command lists have finished execution
	if (directCommandAllocator[backBufferIndex]->Reset() != S_OK)
		FreeAndExit("Can't reset command allocator");

	// Can be done as soon as execute has been called
	if (commandList->Reset(directCommandAllocator[backBufferIndex], pipelineState) != S_OK)
		FreeAndExit("Can't reset command list");

	// Set command list state.
	commandList->SetGraphicsRootSignature(rootSignature);
	commandList->SetDescriptorHeaps(1, &srvHeaps[0]);
	commandList->SetGraphicsRootDescriptorTable(0, srvHeaps[0]->GetGPUDescriptorHandleForHeapStart());

	/* Set scissor rectangle */ {
		D3D12_RECT scissorRect;
		scissorRect.left = 0;
		scissorRect.top = 0;
		scissorRect.right = WIDTH;
		scissorRect.bottom = HEIGHT;
		commandList->RSSetScissorRects(1, &scissorRect);
	}

	/* Set viewport */ {
		D3D12_VIEWPORT viewport;
		viewport.TopLeftX = 0.0f;
		viewport.TopLeftY = 0.0f;
		viewport.Width = (float)WIDTH;
		viewport.Height = (float)HEIGHT;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;
		commandList->RSSetViewports(1, &viewport);
	}

	D3D12_RESOURCE_BARRIER barrier;
	/* Insert barrier to block until render target is available */ {
		barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
		barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
		barrier.Transition.pResource = renderTargets[backBufferIndex];
		barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
		barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
		commandList->ResourceBarrier(1, &barrier);
	}
	
	// Set render target and depth buffer
	D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptor = rtvHeap->GetCPUDescriptorHandleForHeapStart();
	rtvCPUDescriptor.ptr += backBufferIndex * rtvDescriptorSize;
	D3D12_CPU_DESCRIPTOR_HANDLE dsvCPUDescriptor = dsvHeap->GetCPUDescriptorHandleForHeapStart();
	commandList->OMSetRenderTargets(1, &rtvCPUDescriptor, FALSE, &dsvCPUDescriptor);

	// Issue clear screen instruction
	static float clearColour[] = { 0.3f, 0.4f, 0.8f, 1.0f };
	commandList->ClearRenderTargetView(rtvCPUDescriptor, clearColour, 0, NULL);
	commandList->ClearDepthStencilView(dsvCPUDescriptor, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, NULL);

	// Issue drawing instructions
	//commandList->IASetVertexBuffers(0, 1, &vertexBufferView);
	//commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//commandList->DrawInstanced(vertexCount, 1, 0, 0);
	commandList->ExecuteBundle(bundle);

	for (int i = 0; i < CUBE_COUNT; i++) {
		commandList->SetDescriptorHeaps(1, &srvHeaps[i]);
		commandList->SetGraphicsRootDescriptorTable(0, srvHeaps[i]->GetGPUDescriptorHandleForHeapStart());
		commandList->ExecuteBundle(bundle);
	}

	// Barrier to transition render target after instructions completed
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	commandList->ResourceBarrier(1, &barrier);

	// That's the end of the command list
	if (commandList->Close() != S_OK)
		FreeAndExit("Couldn't close command list");
}

void ResetNavigation()
{
	heading = 0.0f;
	elevation = 0.0f;
	cameraPosition = { 0.0f, 0.0f, 4.0f };
}

void Update(double timeSecs, double deltaLastFrame)
{
	if (keyMap[VK_ESCAPE])
		PostMessage(MainWindowHandle, WM_CLOSE, 0, 0);
	if (keyMap['R']) {
		ResetNavigation();
		keyMap['R'] = false;
	}

	const double TURN_RATE = 0.1f*deltaLastFrame, MOVE_RATE = deltaLastFrame;
	if (keyMap[VK_UP]) elevation += TURN_RATE;
	if (keyMap[VK_DOWN]) elevation -= TURN_RATE;
	if (keyMap[VK_LEFT]) heading += TURN_RATE;
	if (keyMap[VK_RIGHT]) heading -= TURN_RATE;
	XMVECTOR forward, up, right;
	forward = XMVector3Transform({ 0.0f, 0.0f, -1.0f }, XMMatrixMultiply(XMMatrixRotationX(elevation), XMMatrixRotationY(heading)));
	right = XMVector3Normalize(XMVector3Cross(forward, { 0.0f, 1.0f, 0.0f }));
	up = XMVector3Cross(right, forward);
	if (keyMap['W']) cameraPosition = XMVectorAdd(cameraPosition, XMVectorScale(forward, MOVE_RATE));
	if (keyMap['S']) cameraPosition = XMVectorSubtract(cameraPosition, XMVectorScale(forward, MOVE_RATE));
	if (keyMap['A']) cameraPosition = XMVectorSubtract(cameraPosition, XMVectorScale(right, MOVE_RATE));
	if (keyMap['D']) cameraPosition = XMVectorAdd(cameraPosition, XMVectorScale(right, MOVE_RATE));
	if (keyMap['Q']) cameraPosition = XMVectorAdd(cameraPosition, XMVectorScale(up, MOVE_RATE));
	if (keyMap['Z']) cameraPosition = XMVectorSubtract(cameraPosition, XMVectorScale(up, MOVE_RATE));
	constantBufferData.view = XMMatrixLookToRH(cameraPosition, forward, up);
	constantBufferData.eyePos = cameraPosition;
	constantBufferData.ptLights[0] = { (float)sin(timeSecs), -(float)cos(timeSecs), -1, 0 };

	constantBufferData.seconds = timeSecs;
	constantBufferData.world = XMMatrixIdentity(); //XMMatrixRotationY(timeSecs)* XMMatrixRotationX(1.4) * XMMatrixTranslation(3.0, 0.0, 0.0) * XMMatrixRotationY(0);
	memcpy(constantBufferMappedPointers[0], &constantBufferData, sizeof(ConstantBuffer));
	//constantBufferData.world = XMMatrixScaling(0.3, 0.3, 0.3) * XMMatrixRotationX(1.4) * XMMatrixTranslationFromVector(constantBufferData.ptLights[0]);
	//constantBufferData.world = XMMatrixRotationY(timeSecs+0.5) * XMMatrixRotationX(1.4) * XMMatrixTranslation(3.0, 0.0, 0.0) * XMMatrixRotationZ(0);
	memcpy(constantBufferMappedPointers[1], &constantBufferData, sizeof(ConstantBuffer));
	/*constantBufferData.world = XMMatrixScaling(0.3, 0.3, 0.3) * XMMatrixRotationX(1.4) * XMMatrixTranslationFromVector(constantBufferData.ptLights[0]);
	memcpy(constantBufferMappedPointers[2], &constantBufferData, sizeof(ConstantBuffer));*/
}

void Render()
{
	FillCommandList();
	ID3D12CommandList *cl[] = { commandList };
	commandQueue->ExecuteCommandLists(1, cl);

	if (swapChain->Present(1, 0) != S_OK)
		FreeAndExit("Couldn't present frame");

	UINT64 fenceValue = bufferFenceValue[backBufferIndex];
	backBufferIndex = swapChain->GetCurrentBackBufferIndex();
	WaitForCommandCompletion(bufferFenceValue[backBufferIndex]);
	bufferFenceValue[backBufferIndex] = fenceValue + 1;
}

void ReportFrameRate(float nowSecs, float timeInterval, int numReports)
{
	// Will issue a total of numReports reports.
	// Set numReports to -1 to report forever, 0 never.
	static int frameCount = -1;
	static int reportCount = 0;
	static float startSecs;

	if (reportCount == numReports) return;

	if (frameCount < 0) {
		startSecs = nowSecs;
		frameCount = 0;
	}

	frameCount++;
	if (nowSecs - startSecs < timeInterval) return;

	float elapsedSecs = (float)(nowSecs - startSecs);
	float fps = ((float)frameCount) / elapsedSecs;
	printf("Time for %d frames is %0.000fs (%0.1f fps)\n", frameCount, elapsedSecs, fps);
	reportCount++;
	startSecs = nowSecs;
	frameCount = 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	switch (message)
	{
	case WM_ERASEBKGND:
		return 0;
	case WM_PAINT:
		BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_KEYDOWN:
		keyMap[wParam & 255] = true;
		break;
	case WM_KEYUP:
		keyMap[wParam & 255] = false;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// Create a console window for debugging purposes
	AllocConsole();
	FILE *outstream, *instream;
	freopen_s(&outstream, "con:", "w", stdout);
	freopen_s(&instream, "con:", "r", stdin);

	// Standard Win32 window creation - make a class then the window
	
	/* Register a window 'class' */ {
		WNDCLASSEXW wcex;
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = NULL;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)NULL;
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = L"DX12ClassV01";
		wcex.hIconSm = NULL;
		RegisterClassExW(&wcex);
	}

	/* Create the window */ {
		// Need to make sure that the client area of the window is of the correct size
		RECT area = { 0, 0, WIDTH, HEIGHT };
		DWORD style = WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
		AdjustWindowRect(&area, style, FALSE);
		printf("Adjusted window area is %d, %d, %d, %d\n", area.left, area.top, area.right, area.bottom);
		MainWindowHandle = CreateWindowW(L"DX12ClassV01", L"RayTwo01", style,
			area.left + 800, area.top + 50, area.right - area.left, area.bottom - area.top,
			nullptr, nullptr, hInstance, nullptr);
		if (MainWindowHandle == NULL) return false;
		ShowWindow(MainWindowHandle, nCmdShow);

		RECT cr;  GetClientRect(MainWindowHandle, &cr);
		printf("Actual window size is %d, %d, %d, %d\n", cr.left, cr.top, cr.right, cr.bottom);
	}

	// Initialise DirectX12 graphics system and load assets
	Init();

	// Time variables and initialisation
	auto startTime = std::chrono::high_resolution_clock::now();
	auto lastFrame = startTime;
	auto now = startTime;
	double timeSeconds;
	auto deltaLastFrame = std::chrono::duration_cast<std::chrono::microseconds>(now - lastFrame);

	// Windows message 'pump' manages flow of user interaction (mouse, keyboard, etc)
	MSG msg;
	while (true) {
		// Process any user interface interactions in the message queue
		// and when there is no interaction (most of the time) update
		// the graphics program and display a frame
		if (PeekMessage(&msg, nullptr, 0, 0, TRUE)) {
			if (msg.message == WM_QUIT) break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			now = std::chrono::high_resolution_clock::now();
			deltaLastFrame = std::chrono::duration_cast<std::chrono::microseconds>(now - lastFrame);
			timeSeconds = std::chrono::duration_cast<std::chrono::microseconds>(now - startTime).count() / 1000000.0;

			Update(timeSeconds, deltaLastFrame.count()/100000.0);
			Render();
			ReportFrameRate(timeSeconds, 5.0f, 5);
			lastFrame = now;
		}
	}

	WaitForCommandCompletion(bufferFenceValue[backBufferIndex]);
	FreeResources();

	return (int)msg.wParam;
}
