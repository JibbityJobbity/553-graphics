#include "ObjReader.h"

// Object File Reader variables
static FILE *OFRFile;
static const UINT OFR_BUFFER_SIZE = 100000;
static const UINT OFR_CHUNK_SIZE = 100000;
static char buffer[OFR_BUFFER_SIZE];
static float OFRPositions[OFR_CHUNK_SIZE][3], OFRNormals[OFR_CHUNK_SIZE][3], OFRTexCoords[OFR_CHUNK_SIZE][2];
static int OFRChunkOffset, OFRChunkLimit;
static UINT OFRBufferFull, OFRBufferNext;
static bool OFRBufferEOF;
static char OFRChar;

static void OFRGet()
{
	if (OFRBufferNext < OFRBufferFull) {
		OFRChar = buffer[OFRBufferNext++];
	}
	else {
		if (feof(OFRFile)) {
			OFRBufferEOF = true;
			OFRChar = 0;
		}
		else {
			OFRBufferFull = (UINT)fread_s(buffer, OFR_BUFFER_SIZE, 1, OFR_BUFFER_SIZE, OFRFile);
			OFRBufferNext = 0;
			OFRGet();
		}
	}
}

static void OFRFlushLine()
{
	while (OFRChar != 0 && OFRChar != '\r' && OFRChar != '\n')
		OFRGet();
	while (OFRChar == '\r' || OFRChar == '\n')
		OFRGet();
}

static bool OFRGetFloat(float *place)
{
	while (OFRChar == ' ')
		OFRGet();
	float f = 0.1f;
	float result = 0.0f;
	bool negative = false;
	if (OFRChar == '-') {
		negative = true;
		OFRGet();
	}
	if (OFRChar < '0' || OFRChar > '9') return false;
	while (OFRChar >= '0' && OFRChar <= '9') {
		result = result * 10.0f + (OFRChar - '0');
		OFRGet();
	}
	if (OFRChar == '.') {
		OFRGet();
		while (OFRChar >= '0' && OFRChar <= '9') {
			result = result + f * (OFRChar - '0');
			f = f / 10.0f;
			OFRGet();
		}
	}
	if (negative) result = -result;
	if (place != NULL) *place = result;
	return true;
}

static bool OFRGetNumber(int *place)
{
	while (OFRChar == ' ')
		OFRGet();
	int result = 0;
	bool negative = false;
	if (OFRChar == '-') {
		negative = true;
		OFRGet();
	}
	if (OFRChar < '0' || OFRChar > '9') return false;
	while (OFRChar >= '0' && OFRChar <= '9') {
		result = result * 10 + (OFRChar - '0');
		OFRGet();
	}
	if (negative) result = -result;
	if (place != NULL) *place = result;
	return true;
}

static bool OFRParseVector(int dim, float *place)
{
	float x;
	for (int i = 0; i < dim; i++) {
		if (!OFRGetFloat(&x))
			return false;
		if (place != NULL)
			*(place + i) = x;
	}
	return true;
}

static bool OFRParseTriple(int *pv, int *pn, int *pt)
{
	if (!OFRGetNumber(pv))
		return false;
	if (OFRChar != '/')
		return false;
	OFRGet();
	if (!OFRGetNumber(pn))
		return false;
	if (OFRChar != '/')
		return false;
	OFRGet();
	if (!OFRGetNumber(pt))
		return false;
	return true;
}

static bool OFRParseFace(UINT &count, float *vbptr)
{
	int iv[4], it[4], in[4];
	int vertices = 0;
	for (;;) {
		while (OFRChar == ' ')
			OFRGet();
		if (OFRChar < '0' || OFRChar > '9')
			break;
		if (vertices > 3) return false;
		if (!OFRParseTriple(&iv[vertices], &it[vertices], &in[vertices]))
			return false;
		iv[vertices]--;  it[vertices]--;  in[vertices]--;
		vertices++;
	}

	if (vbptr != NULL) {
		float *fptr = vbptr + (8 * 3 * count);
		// vertices 0, 1, 2
		for (int v = 0; v <= 2; v++) {
			if (iv[v] >= OFRChunkOffset && iv[v] < OFRChunkLimit) {
				*(fptr + 0) = OFRPositions[iv[v] - OFRChunkOffset][0];
				*(fptr + 1) = OFRPositions[iv[v] - OFRChunkOffset][1];
				*(fptr + 2) = OFRPositions[iv[v] - OFRChunkOffset][2];
			}
			if (in[v] >= OFRChunkOffset && in[v] < OFRChunkLimit) {
				*(fptr + 3) = OFRNormals[in[v] - OFRChunkOffset][0];
				*(fptr + 4) = OFRNormals[in[v] - OFRChunkOffset][1];
				*(fptr + 5) = OFRNormals[in[v] - OFRChunkOffset][2];
			}
			if (it[v] >= OFRChunkOffset && it[v] < OFRChunkLimit) {
				*(fptr + 6) = OFRTexCoords[it[v] - OFRChunkOffset][0];
				*(fptr + 7) = OFRTexCoords[it[v] - OFRChunkOffset][1];
			}
			fptr += 8;
		}
		if (vertices == 4) {
			// vertices 2, 3, 0
			for (int v = 2; v != 1; v = (v + 1) % 4) {
				if (iv[v] >= OFRChunkOffset && iv[v] < OFRChunkLimit) {
					*(fptr + 0) = OFRPositions[iv[v] - OFRChunkOffset][0];
					*(fptr + 1) = OFRPositions[iv[v] - OFRChunkOffset][1];
					*(fptr + 2) = OFRPositions[iv[v] - OFRChunkOffset][2];
				}
				if (in[v] >= OFRChunkOffset && in[v] < OFRChunkLimit) {
					*(fptr + 3) = OFRNormals[in[v] - OFRChunkOffset][0];
					*(fptr + 4) = OFRNormals[in[v] - OFRChunkOffset][1];
					*(fptr + 5) = OFRNormals[in[v] - OFRChunkOffset][2];
				}
				if (it[v] >= OFRChunkOffset && it[v] < OFRChunkLimit) {
					*(fptr + 6) = OFRTexCoords[it[v] - OFRChunkOffset][0];
					*(fptr + 7) = OFRTexCoords[it[v] - OFRChunkOffset][1];
				}
				fptr += 8;
			}
		}
	}

	if (vertices == 3)
		count += 1;
	else
		count += 2;
	return true;
}

bool OFR1111CheckAndCountParser(FILE *file, UINT &sizeRequiredFloats, UINT &reqdChunkSpace)
// Requires tri/quad file with vertex/normal/texture per vertex
// Parses file (returning false on syntax error), and calculates
// size of vertex buffer required (in floats)
{
	OFRFile = file;
	bool ok = true;
	UINT cu = 0, cv = 0, cn = 0, ct = 0, cf = 0;
	OFRBufferEOF = false;  OFRBufferFull = 0;  OFRBufferNext = 0; OFRGet();
	while (ok && !OFRBufferEOF) {
		switch (OFRChar) {
		case '#': case 'm': case 'g': case 'u': case '\n': case '\r':
			OFRFlushLine();
			break;
		case 'v':
			OFRGet();
			if (OFRChar == 'n') {  // Normal
				OFRGet();
				if (!OFRParseVector(3, NULL))
					ok = false;
				cn++;
			}
			else if (OFRChar == 't') {  // Texture Coordinates
				OFRGet();
				if (!OFRParseVector(2, NULL))
					ok = false;
				ct++;
			}
			else {                 // Vertex Position
				if (!OFRParseVector(3, NULL))
					ok = false;
				cv++;
			}
			OFRFlushLine();
			break;
		case 'f':
			OFRGet();
			if (!OFRParseFace(cf, NULL))
				ok = false;
			OFRFlushLine();
			break;
		default:
			cu++;
			//printf("Code letter was %c\n", OFRChar);
			OFRFlushLine();
			break;
		}
	}
	sizeRequiredFloats = (size_t)(cf * 3 * (3 + 3 + 2));
	reqdChunkSpace = cv;
	if (cn > reqdChunkSpace) reqdChunkSpace = cn;
	if (ct > reqdChunkSpace) reqdChunkSpace = ct;
	//printf("vertex lines = %d\n", cv);
	//printf("normal lines = %d\n", cn);
	//printf("texcoord lines = %d\n", ct);
	//printf("face lines = %d\n", cf);
	//printf("unknown lines = %d\n", cu);
	return ok;
}

static void OFRCollectParser()
// Assumes syntactically correct file, collects vertex, normal
// and texture coordinate data over the chunk range set globally
{
	int cv = 0, cn = 0, ct = 0;
	OFRBufferEOF = false;  OFRBufferFull = 0;  OFRBufferNext = 0; OFRGet();
	while (!OFRBufferEOF) {
		switch (OFRChar) {
		case '#': case 'm': case 'g': case 'u': case '\n': case '\r':
			OFRFlushLine();
			break;
		case 'v':
			OFRGet();
			if (OFRChar == 'n') {  // Normal
				OFRGet();
				OFRParseVector(3, cn >= OFRChunkOffset && cn < OFRChunkLimit ? &OFRNormals[cn - OFRChunkOffset][0] : NULL);
				cn++;
			}
			else if (OFRChar == 't') {  // Texture Coordinates
				OFRGet();
				OFRParseVector(2, ct >= OFRChunkOffset && ct < OFRChunkLimit ? &OFRTexCoords[ct - OFRChunkOffset][0] : NULL);
				ct++;
			}
			else {                 // Vertex Position
				OFRParseVector(3, cv >= OFRChunkOffset && cv < OFRChunkLimit ? &OFRPositions[cv - OFRChunkOffset][0] : NULL);
				cv++;
			}
			OFRFlushLine();
			break;
		case 'f':
			OFRFlushLine();
			break;
		default:
			OFRFlushLine();
			break;
		}
	}
}

static void OFRFillParser(float *vertexBufferPtr)
// Assumes syntactically correct file, finds face definitions and 
// fills data from current chunk range into vertex buffer
{
	UINT cf = 0;
	OFRBufferEOF = false;  OFRBufferFull = 0;  OFRBufferNext = 0; OFRGet();
	while (!OFRBufferEOF) {
		switch (OFRChar) {
		case '#': case 'm': case 'g': case 'u': case '\n': case '\r':
			OFRFlushLine();
			break;
		case 'v':
			OFRFlushLine();
			break;
		case 'f':
			OFRGet();
			OFRParseFace(cf, vertexBufferPtr);
			OFRFlushLine();
			break;
		default:
			OFRFlushLine();
			break;
		}
	}
}

void OFR2222CollectAndFillParser(FILE *file, int reqdChunkSpace, float *vertexBufferPtr)
{
	OFRFile = file;
	OFRChunkOffset = 0;
	for (OFRChunkOffset = 0; OFRChunkOffset < reqdChunkSpace; OFRChunkOffset += OFR_CHUNK_SIZE) {
		OFRChunkLimit = OFRChunkOffset + OFR_CHUNK_SIZE;

		fseek(OFRFile, 0, SEEK_SET);
		OFRCollectParser();

		fseek(OFRFile, 0, SEEK_SET);
		OFRFillParser(vertexBufferPtr);
	}
}
