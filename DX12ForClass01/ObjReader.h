#pragma once

#include "Headers.h"

// The external program is responsible for opening the closing the obj file.
// It must open the file and call the first pass, with the file as a parameter.
// The first pass returns some size information.  The external program must
// allocate buffer space and then call the second pass with the still open file,
// a pointer to the vertex buffer memory and returning the 'chunk space' size 
// parameter.  The second pass will reread the file and pack data into the 
// vertex buffer.  A vertex buffer with position, normal and texture coordinates
// is assumed.

// First pass, counts components so that memory can be allocated later
bool OFR1111CheckAndCountParser(FILE *file, UINT &sizeRequiredFloats, UINT &reqdChunkSpace);

// Second pass - collect and put information into the vertex buffer provided
void OFR2222CollectAndFillParser(FILE *file, int reqdChunkSpace, float *vertexBufferPtr);
